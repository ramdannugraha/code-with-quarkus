package org.acme.commandmode;


import io.smallrye.mutiny.Multi;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/test")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TestController {

    @Inject
    TestService testService;

    @GET
    public Multi<Testdata> getAllData() {
        return Multi.createFrom().items(this.testService::findAll);
    }

}
