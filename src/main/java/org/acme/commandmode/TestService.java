package org.acme.commandmode;

import javax.enterprise.context.ApplicationScoped;
import java.util.stream.Stream;

@ApplicationScoped
public class TestService {

    private final TestRepository testRepository;

    public TestService(TestRepository testRepository) {
        this.testRepository = testRepository;
    }

    public Stream<Testdata> findAll() {
        return this.testRepository.findAll().stream();
    }

}
